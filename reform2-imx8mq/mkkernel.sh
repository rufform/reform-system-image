#!/bin/bash

set -x
set -e

export ARCH=arm64
export KBUILD_BUILD_TIMESTAMP=$(date --date="@$SOURCE_DATE_EPOCH" --rfc-email)
export KBUILD_BUILD_VERSION=1
export KBUILD_BUILD_USER=reformuser
export KBUILD_BUILD_HOST=reformhost

if [ "$(uname -m)" != "aarch64" ]
then
	export CROSS_COMPILE=aarch64-linux-gnu-
fi

if [ ! -d linux ]
then
  echo "Cloning Linux..."
  git clone --depth 1 --branch pureos/byzantium https://source.puri.sm/Librem5/linux-next.git linux
fi

cp ./template-kernel/*.dts ./linux/arch/arm64/boot/dts/freescale/
cp ./template-kernel/kernel-config ./linux/.config

cd linux

echo "== kernel config =="
cat .config
echo "== end kernel config =="

for PATCHFILE in ../template-kernel/patches/*.patch
do
  echo PATCH: $PATCHFILE
  if git apply --check $PATCHFILE; then
    git apply $PATCHFILE
  else
    echo "\e[1mKernel patch already applied or cannot apply: $PATCHFILE"
  fi
done

make -j$(nproc) Image freescale/imx8mq-mnt-reform2.dtb freescale/imx8mq-mnt-reform2-hdmi.dtb
make -j$(nproc) modules

cd ..
tar czf kmods.tgz -C linux modules.builtin.modinfo modules.builtin modules.order `cat linux/modules.order`
