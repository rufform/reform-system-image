#!/bin/sh

set -x
set -e

if [ ! -d u-boot ]
then
  echo "Cloning U-Boot..."
  git clone --depth 1 https://source.mnt.re/reform/reform-boundary-uboot.git u-boot
fi

cd u-boot
cp mntreform-config .config

export CROSS_COMPILE=aarch64-linux-gnu-
export ARCH=arm

# build rescue u-boot first (loads kernel from eMMC)
make -j$(nproc) flash.bin KCPPFLAGS='-DMNTREFORM_BOOT_EMMC'
cp flash.bin flash-rescue.bin

# build normal u-boot second (loads kernel from SD card)
make -j$(nproc) flash.bin

cd ..
