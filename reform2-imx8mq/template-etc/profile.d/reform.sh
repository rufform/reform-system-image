# Defaults for MNT Reform

# enable NIR shader path in mesa. without this,
# some Xwayland applications will be blank
export ETNA_MESA_DEBUG=nir

# set GTK2 theme
export GTK2_RC_FILES=/usr/share/themes/Arc-Dark/gtk-2.0/gtkrc

unicode_start
